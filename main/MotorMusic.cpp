#include "MotorMusic.h"
#include "arduino.h"

int MusicCount = 0; 
void MotorSound(int FreqCount)
{
  if (FreqCount == -1)
  {
    analogWriteFreq(200);
  }
  else{
   switch (FreqCount % 7)
   {
    case 0:
      analogWriteFreq(262 * (int)(FreqCount/7 + 1));
      break;    
    case 1:
      analogWriteFreq(294 * (int)(FreqCount/7 + 1));
      break;    
    case 2:
      analogWriteFreq(330 * (int)(FreqCount/7 + 1));
      break;    
    case 3:
      analogWriteFreq(349 * (int)(FreqCount/7 + 1));
      break;    
    case 4:
      analogWriteFreq(392 * (int)(FreqCount/7 + 1));
      break;    
    case 5:
      analogWriteFreq(440 * (int)(FreqCount/7 + 1));
      break;    
    case 6:
      analogWriteFreq(494 * (int)(FreqCount/7 + 1));
      break;   
   }    
 }
}
short MusicScore[72]={
  7,6,5,-1,
  7,6,5,-1,
  7,6,5,4,5,6,7,6,5,-1,
  
  7,6,5,-1,
  7,6,5,-1,
  7,6,5,4,5,6,7,6,5,-1,

  8,7,6,-1,
  8,7,6,-1,
  8,7,6,5,6,7,8,7,6,-1,
  
  8,7,6,-1,
  8,7,6,-1,
  8,7,6,5,6,7,8,7,6,-1,

  
};
void Music(void)
{
  MotorSound(MusicScore[MusicCount]);
  MusicCount++;
  
  if(MusicCount >= 72)
  {
    MusicCount = 0;
  }
  
}
