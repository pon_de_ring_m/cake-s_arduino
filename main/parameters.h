#ifndef _PARAMETERS
#define _PARAMETERS
    static const char *ssid = "承久のLAN";
    static const int TAIL_MAX = 160;
    static const int TAIL_DEFAULT = 160;

    static const char *password = "takadamalab";
    static const char *host = "192.168.43.1";
    static const int port = 8080;

    static const int LMotorIN1 = 15;
    static const int LMotorIN2 = 13;

    static const int RMotorIN1 = 14;
    static const int RMotorIN2 = 12;

    static const int T_SERVO_PIN = 4;
    
    static const int P_SERVO_PIN = 5 ;

    static const int LED_PIN = 0;
    static const int NUM_LEDS = 7;

    static const int LED_STATUS = 2;
//////////////// ip-address ////////////////
IPAddress ip(192, 168, 43, 100);
IPAddress gateway(192, 168, 43, 1);
IPAddress subnet(255, 255, 255, 0);
//////////////////////////////////////////

#endif

