#define OCT 2
#include <WiFiServer.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <Servo.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include "MotorDrive_DRV8835.h"
#include <Ticker.h>
#include "parameters.h"
#include "MotorMusic.h"

extern "C" {
#include "user_interface.h"
} //ADC setting


boolean FirstConnectFlag = false;
boolean BatteryVoltageFlag = false;
boolean MusicFlag = true;
boolean PingFlag;

Servo T_servo;
Servo P_servo;

Motor_DRV8835 Motor_R(RMotorIN1, RMotorIN2);
Motor_DRV8835 Motor_L(LMotorIN1, LMotorIN2);

String COMMAND = "";
String VALUE = "";

unsigned long nowtime;

WiFiClient client;
Ticker ReadBatteryVoltageTicker; 
int BatteryVoltage;
void ReadBatteryVoltage(void)
{
 BatteryVoltage = system_adc_read();
 BatteryVoltageFlag = true;
 PingFlag = true;
}

Ticker MusicTicker; 


void setup() {
  // delete old config
  WiFi.disconnect(true);
  Motor_L.Brake();
  Motor_R.Brake();
  delay(1000);
  
  pinMode(LED_STATUS, OUTPUT);
  digitalWrite(LED_STATUS, HIGH);

  T_servo.write(TAIL_DEFAULT);
  T_servo.attach(T_SERVO_PIN);
  P_servo.write(0);
  P_servo.attach(P_SERVO_PIN);
  

  Serial.begin(115200);
  Serial.println("Booting");
  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  WiFi.config( ip, gateway, subnet );
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
 
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  ReadBatteryVoltage();
  ReadBatteryVoltageTicker.attach_ms(1000, ReadBatteryVoltage);

  wifi_set_sleep_type(MODEM_SLEEP_T);

  MusicTicker.attach_ms(300, Music);
  MusicFlag = true;
  Serial.println("set up finish");


  digitalWrite(LED_STATUS, LOW);
  nowtime = millis();
}

void loop() {
  client.setNoDelay(true);
  while(!client.connect(host, port))
  {    
    ArduinoOTA.handle();
    delay(1);
  }
  boolean CommandTimeOutLED = false;
  while (client.connected())
  {    
    delay(1);
    if(millis() - nowtime > 60000)
    {
      Motor_L.Drive(0);
      Motor_R.Drive(0);

      nowtime = millis();
    }

    if(BatteryVoltageFlag == true)
    {

      if(CommandTimeOutLED == false)
      {
        CommandTimeOutLED = true;
        digitalWrite(LED_STATUS, LOW);
      }
      else
      {
        CommandTimeOutLED = false;
        digitalWrite(LED_STATUS, HIGH);
      }
      BatteryVoltageFlag = false;
    }
      
    
    while (client.available())
    {
              
      String res = client.readStringUntil('\n');
      
      client.flush();

      COMMAND = res.substring(0, res.indexOf(' '));
      VALUE = res.substring(res.indexOf(' ') + 1, res.length());
      if (!COMMAND.equals("ping"))
      {
         nowtime = millis(); 
      }

      if (COMMAND.equals("ping"))
      {
        
       }
       
      else if (COMMAND.equals("l_motor"))
      {
        if (VALUE.equals("s"))
        {
          Motor_L.Brake();
        }
        else 
        {
          Motor_L.Drive(VALUE.toInt());
        }
      }
      else if (COMMAND.equals("r_motor"))
      {
        if (VALUE.equals("s"))
        {
          Motor_R.Brake();
        }
        else 
        {
          Motor_R.Drive(VALUE.toInt());
        }
      }
      else if (COMMAND.equals("t_servo"))
      {
          if(VALUE.toInt()>TAIL_MAX)
          {
            T_servo.write(TAIL_MAX);
          }else{
            T_servo.write(VALUE.toInt());
          
        }
      }
      else if (COMMAND.equals("p_servo"))
      {
        P_servo.write(VALUE.toInt());
      } 
      else if (COMMAND.equals("reboot"))
      {  
        client.stop();
        ESP.restart();
      }
      else if (COMMAND.equals("freq"))
      {
        MusicTicker.detach();
        MusicFlag = false;
        analogWriteFreq(VALUE.toInt());
      }
      else if (COMMAND.equals("music"))
      {
        if (VALUE.equals("start") && MusicFlag == false)
        {
          MusicTicker.attach_ms(62, Music);
          MusicFlag = true;
        }
        else if (VALUE.equals("stop") && MusicFlag == true)
        {
          MusicTicker.detach();
          MusicFlag = false;
        }
      }
      else if (COMMAND.equals("sleep"))
      {
        client.stop();
        WiFi.disconnect();
        wifi_set_sleep_type(LIGHT_SLEEP_T);
        delay(VALUE.toInt());
        wifi_set_sleep_type(MODEM_SLEEP_T);

        ESP.restart();
          
        
      }
      else if(COMMAND.equals("deepsleep"))
      {
        if( VALUE.toInt() >= 1000)
        {
        ESP.deepSleep(VALUE.toInt() * 1000, WAKE_RF_DEFAULT);
        delay(1000);
        }
      }

      else
      {
      }
      
    }
 if(!client.available())
  {
    if(PingFlag == true)
    {
      client.println(BatteryVoltage);
      delay(10);
      PingFlag = false;
    }
    else
    {
      client.println("@");
      delay(10);
    }
  }
     
  ESP.wdtFeed();
  }
  client.stop();
}


