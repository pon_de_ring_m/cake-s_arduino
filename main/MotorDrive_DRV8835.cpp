#include "MotorDrive_DRV8835.h"
#include "arduino.h"

Motor_DRV8835::Motor_DRV8835(int pin1, int pin2)
{
    MotorIN1 = pin1;
    MotorIN2 = pin2;
    pinMode(pin1, OUTPUT);
    pinMode(pin2, OUTPUT);
    
}

void Motor_DRV8835::Drive(int power)
{
    if (power > 0)
    {
      analogWrite(MotorIN1, abs(power));
      analogWrite(MotorIN2, 0);
    }
    else
    {
      analogWrite(MotorIN1, 0);
      analogWrite(MotorIN2, abs(power));
    }
}
void Motor_DRV8835::Brake()
{
  {
    analogWrite(MotorIN1, 1000);
    analogWrite(MotorIN2, 1000);

  }
}
void Motor_DRV8835::ChangeFreq(int freq)
{
  analogWriteFreq(freq);
}



