#ifndef _MOTOR_DRIVE
#define _MOTOR_DRIVE
class Motor_DRV8835
{
  private:
  public:
    int MotorIN1,MotorIN2;
    Motor_DRV8835(int pin1, int pin2);
    void Drive(int power);
    void Brake();
    void ChangeFreq(int freq);
 
};

#endif

