メインプログラムは

cake-s_arduino / main / main.ino

です

# 命令一覧

+ ping
+ l_motor ( -1000 ~ 1000 )
+ r_motor ( -1000 ~ 1000 )
+ t_servo ( 0 ~ 180 )
+ p_servo ( 0 ~ 180 )
+ reboot
+ freq
+ music
    + start
    + stop
+ sleep (0 ~ )
+ deepsleep(0 ~ )